<?php

$vendorDir = dirname(__DIR__);

return array (
  'mervick/yii2-material-design-icons' => 
  array (
    'name' => 'mervick/yii2-material-design-icons',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@yii/materialicons' => $vendorDir . '/mervick/yii2-material-design-icons',
    ),
  ),
  'deyraka/yii2-material-dashboard' => 
  array (
    'name' => 'deyraka/yii2-material-dashboard',
    'version' => '1.0.0.0-beta',
    'alias' => 
    array (
      '@deyraka/materialdashboard' => $vendorDir . '/deyraka/yii2-material-dashboard',
    ),
  ),
  'miloschuman/yii2-highcharts-widget' => 
  array (
    'name' => 'miloschuman/yii2-highcharts-widget',
    'version' => '9.0.0.0',
    'alias' => 
    array (
      '@miloschuman/highcharts' => $vendorDir . '/miloschuman/yii2-highcharts-widget/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.11.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-bootstrap4' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap4',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap4' => $vendorDir . '/yiisoft/yii2-bootstrap4/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.18.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.2.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
);

/*
Navicat MySQL Data Transfer

Source Server         : hi_server
Source Server Version : 50568
Source Host           : 192.168.111.5:3306
Source Database       : hi

Target Server Type    : MYSQL
Target Server Version : 50568
File Encoding         : 65001

Date: 2022-07-04 14:17:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for `report_er_dx`
-- ----------------------------
DROP VIEW IF EXISTS `report_er_dx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_er_dx` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`vn`) AS `total` from (((`ovst` `o` join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`o`.`an` = 0) and (`o`.`cln` = '20100') and (if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`o`.`vn`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_error_mdr`
-- ----------------------------
DROP VIEW IF EXISTS `report_error_mdr`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_error_mdr` AS select cast(`o`.`vstdttm` as date) AS `date_serv`,count(distinct (case when (isnull(`x`.`vn`) and (`i`.`vn` is not null)) then `o`.`vn` end)) AS `total_no_dx`,count(distinct (case when (isnull(`i`.`vn`) and (`x`.`vn` is not null)) then `o`.`vn` end)) AS `total_no_cost`,count(distinct (case when (isnull(`i`.`vn`) and isnull(`x`.`vn`)) then `o`.`vn` end)) AS `total_no_dx_no_cost` from ((`ovst` `o` left join `incoth` `i` on((`o`.`vn` = `i`.`vn`))) left join `ovstdx` `x` on((`o`.`vn` = `x`.`vn`))) where (`o`.`cln` <> '40100') group by cast(`o`.`vstdttm` as date) ;

-- ----------------------------
-- View structure for `report_fs`
-- ----------------------------
DROP VIEW IF EXISTS `report_fs`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_fs` AS select `s`.`hospmain` AS `hospmain`,`o`.`hn` AS `hn`,`o`.`vn` AS `vn`,cast(`o`.`vstdttm` as date) AS `service_date`,if(((`a`.`vn` is not null) and (`l`.`vn` is not null)),1000,if((`a`.`vn` is not null),400,0)) AS `anc`,if((`v`.`vn` is not null),40,0) AS `vaccine`,if((`p`.`vn` is not null),1000,0) AS `paliative`,if((`u`.`vn` is not null),600,0) AS `ultrasound`,if((`y`.`vn` is not null),2500,0) AS `norplant`,if((`t1`.`dn` is not null),200,0) AS `dental1`,if((`t2`.`dn` is not null),300,0) AS `dental2`,((((((if(((`a`.`vn` is not null) and (`l`.`vn` is not null)),1000,if((`a`.`vn` is not null),400,0)) + if((`v`.`vn` is not null),40,0)) + if((`p`.`vn` is not null),1000,0)) + if((`u`.`vn` is not null),600,0)) + if((`y`.`vn` is not null),2500,0)) + if((`t1`.`dn` is not null),200,0)) + if((`t2`.`dn` is not null),300,0)) AS `total` from (((((((((((`ovst` `o` join `pt` on((`o`.`hn` = `pt`.`hn`))) left join `insure` `s` on(((`o`.`hn` = `s`.`hn`) and (`o`.`pttype` = `s`.`pttype`)))) left join `ovstdx` `a` on(((`o`.`vn` = `a`.`vn`) and (`a`.`icd10` in ('Z340','Z348')) and (`pt`.`male` = 2)))) left join `ovstdx` `v` on(((`o`.`vn` = `v`.`vn`) and (`v`.`icd10` = 'U119')))) left join `ovstdx` `p` on(((`o`.`vn` = `p`.`vn`) and (`p`.`icd10` = 'Z515')))) left join `oprt` `u` on(((`o`.`vn` = `u`.`vn`) and (`u`.`codeicd9id` in (169,171))))) left join `oprt` `y` on(((`o`.`vn` = `y`.`vn`) and (`y`.`codeicd9id` = 392)))) left join `dt` on(((`o`.`vn` = `dt`.`vn`) and (`dt`.`grp` = '04')))) left join `dtdx` `t1` on(((`dt`.`dn` = `t1`.`dn`) and (`t1`.`dttx` in ('2330011','2330013'))))) left join `dtdx` `t2` on(((`dt`.`dn` = `t2`.`dn`) and (`t2`.`dttx` in (2387010,2277310,2277320,2287310,2287320))))) left join `lbbk` `l` on((`a`.`vn` = `l`.`vn`))) where ((`o`.`pttype` = '15') and (cast(`o`.`vstdttm` as date) >= '2021-10-01')) group by `o`.`vn` having (`total` > 0) ;

-- ----------------------------
-- View structure for `report_grouppdx`
-- ----------------------------
DROP VIEW IF EXISTS `report_grouppdx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_grouppdx` AS select if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),year((`o`.`vstdttm` + interval 1 year))) AS `yearbudget`,month(`o`.`vstdttm`) AS `m`,(case when (month(`o`.`vstdttm`) between 10 and 12) then 1 when (month(`o`.`vstdttm`) between 1 and 3) then 2 when (month(`o`.`vstdttm`) between 4 and 6) then 3 when (month(`o`.`vstdttm`) between 7 and 9) then 4 end) AS `trimas`,`l`.`cgroup` AS `group_dx`,`l`.`icd10` AS `icd10`,`l`.`e_name` AS `group_name`,count(distinct `o`.`vn`) AS `total` from (((`ovst` `o` join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1)))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `l` on((convert(`c`.`group_298_disease` using utf8) = `l`.`cgroup`))) group by if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),year((`o`.`vstdttm` + interval 1 year))),month(`o`.`vstdttm`),`l`.`cgroup` ;

-- ----------------------------
-- View structure for `report_ip_dx`
-- ----------------------------
DROP VIEW IF EXISTS `report_ip_dx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_ip_dx` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`an`) AS `total` from (((`ipt` `o` join `iptdx` `x` on(((`o`.`an` = `x`.`an`) and (`x`.`itemno` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where (if((month(`o`.`dchdate`) < 10),year(`o`.`dchdate`),(year(`o`.`dchdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1))) group by `g`.`cgroup` order by count(`o`.`an`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_ip_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_ip_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_ip_visit` AS select if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) AS `yearbudget`,count(distinct `i`.`hn`) AS `people`,count(`i`.`an`) AS `admit`,sum(`i`.`daycnt`) AS `losd`,(timestampdiff(DAY,min(`i`.`dchdate`),max(`i`.`dchdate`)) + 1) AS `days`,((sum(`i`.`daycnt`) * 100) / (30 * (timestampdiff(DAY,min(`i`.`dchdate`),max(`i`.`dchdate`)) + 1))) AS `bed_rate` from `ipt` `i` group by if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) order by if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_op_diag`
-- ----------------------------
DROP VIEW IF EXISTS `report_op_diag`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_op_diag` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`vn`) AS `total` from (((`ovst` `o` join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`o`.`an` = 0) and (if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`o`.`vn`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_op_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_op_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_op_visit` AS select if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) AS `yearbudget`,count(distinct (case when ((`o`.`dct` <> '') or (`o`.`cln` = '40100') or (`o`.`dct` is not null)) then `o`.`hn` end)) AS `people`,count(distinct (case when ((`o`.`dct` <> '') or (`o`.`cln` = '40100') or (`o`.`dct` is not null)) then `o`.`vn` end)) AS `visit`,count(distinct (case when (`o`.`cln` = '40100') then `o`.`vn` end)) AS `visit_dent`,count(distinct (case when (((`o`.`dct` <> '') or (`o`.`dct` is not null)) and (`o`.`cln` = '20100')) then `o`.`vn` end)) AS `visit_er`,count(distinct (case when (((`o`.`dct` <> '') or (`o`.`dct` is not null)) and (`o`.`cln` = '10100')) then `o`.`vn` end)) AS `visit_opd` from `ovst` `o` where (`o`.`an` = 0) group by if((month(`o`.`vstdttm`) < 10),(year(`o`.`vstdttm`) + 543),(year(`o`.`vstdttm`) + 544)) order by if((month(`o`.`vstdttm`) < 10),(year(`o`.`vstdttm`) + 543),(year(`o`.`vstdttm`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_other_pp`
-- ----------------------------
DROP VIEW IF EXISTS `report_other_pp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_other_pp` AS select `s`.`hospmain` AS `hospmain`,`o`.`hn` AS `hn`,`o`.`vn` AS `vn`,cast(`o`.`vstdttm` as date) AS `service_date`,if(((`a`.`vn` is not null) and (`l`.`vn` is not null)),1000,if((`a`.`vn` is not null),400,0)) AS `anc`,if((`v`.`vn` is not null),40,0) AS `vaccine`,if((`p`.`vn` is not null),1000,0) AS `paliative`,if((`u`.`vn` is not null),600,0) AS `ultrasound`,if((`y`.`vn` is not null),2500,0) AS `norplant`,if((`t1`.`dn` is not null),200,0) AS `dental1`,if((`t2`.`dn` is not null),300,0) AS `dental2`,((((((if(((`a`.`vn` is not null) and (`l`.`vn` is not null)),1000,if((`a`.`vn` is not null),400,0)) + if((`v`.`vn` is not null),40,0)) + if((`p`.`vn` is not null),1000,0)) + if((`u`.`vn` is not null),600,0)) + if((`y`.`vn` is not null),2500,0)) + if((`t1`.`dn` is not null),200,0)) + if((`t2`.`dn` is not null),300,0)) AS `total` from (((((((((((`ovst` `o` join `pt` on((`o`.`hn` = `pt`.`hn`))) left join `insure` `s` on(((`o`.`hn` = `s`.`hn`) and (`o`.`pttype` = `s`.`pttype`)))) left join `ovstdx` `a` on(((`o`.`vn` = `a`.`vn`) and (`a`.`icd10` in ('Z340','Z348')) and (`pt`.`male` = 2)))) left join `ovstdx` `v` on(((`o`.`vn` = `v`.`vn`) and (`v`.`icd10` = 'U119')))) left join `ovstdx` `p` on(((`o`.`vn` = `p`.`vn`) and (`p`.`icd10` = 'Z515')))) left join `oprt` `u` on(((`o`.`vn` = `u`.`vn`) and (`u`.`codeicd9id` in (169,171))))) left join `oprt` `y` on(((`o`.`vn` = `y`.`vn`) and (`y`.`codeicd9id` = 392)))) left join `dt` on(((`o`.`vn` = `dt`.`vn`) and (`dt`.`grp` = '04')))) left join `dtdx` `t1` on(((`dt`.`dn` = `t1`.`dn`) and (`t1`.`dttx` in ('2330011','2330013'))))) left join `dtdx` `t2` on(((`dt`.`dn` = `t2`.`dn`) and (`t2`.`dttx` in (2387010,2277310,2277320,2287310,2287320))))) left join `lbbk` `l` on((`a`.`vn` = `l`.`vn`))) where ((`o`.`pttype` = '15') and (cast(`o`.`vstdttm` as date) >= '2021-10-01')) group by `o`.`vn` having (`total` = 0) ;

-- ----------------------------
-- View structure for `report_refer`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_refer` AS select if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) AS `yearbudget`,count(`r`.`rfrno`) AS `total`,count((case when (`r`.`loads` between 1 and 3) then `r`.`rfrno` end)) AS `ambulance`,count((case when (`r`.`an` > 0) then `r`.`rfrno` end)) AS `ipd`,count((case when (`r`.`an` = 0) then `r`.`rfrno` end)) AS `opd` from `orfro` `r` group by if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) order by if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_refer_ip`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer_ip`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_refer_ip` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`r`.`rfrno`) AS `total` from ((`orfro` `r` join `icd101` `c` on(((`r`.`icd10` = `c`.`icd10`) and (`r`.`icd10` not between 'V00' and 'Y9999') and (not((`r`.`icd10` like 'Z%')))))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`r`.`an` > 0) and (if((month(`r`.`vstdate`) < 10),year(`r`.`vstdate`),(year(`r`.`vstdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`r`.`rfrno`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_refer_op`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer_op`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_refer_op` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`r`.`rfrno`) AS `total` from ((`orfro` `r` join `icd101` `c` on(((`r`.`icd10` = `c`.`icd10`) and (`r`.`icd10` not between 'V00' and 'Y9999') and (not((`r`.`icd10` like 'Z%')))))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`r`.`an` = 0) and (if((month(`r`.`vstdate`) < 10),year(`r`.`vstdate`),(year(`r`.`vstdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`r`.`rfrno`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_stock`
-- ----------------------------
DROP VIEW IF EXISTS `report_stock`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_stock` AS select `t`.`meditem` AS `meditem`,`p`.`prscdate` AS `prscdate`,if(isnull(`a`.`qty`),0,`a`.`qty`) AS `recieve`,count(distinct `o`.`vn`) AS `patient`,group_concat(`o`.`hn`,':',`d`.`qty` separator ',') AS `dispend`,sum(`d`.`qty`) AS `total`,((`t`.`initial_storage` + sum(distinct ifnull(`a`.`qty`,0))) - sum(`d`.`qty`)) AS `remain` from ((((`ovst` `o` join `prsc` `p` on((`o`.`vn` = `p`.`vn`))) join `prscdt` `d` on((`p`.`prscno` = `d`.`prscno`))) join `stock_item` `t` on((`t`.`meditem` = `d`.`meditem`))) left join `stock_add` `a` on(((`p`.`prscdate` = `a`.`date_recieve`) and (`t`.`item_id` = `a`.`item_id`)))) where ((cast(`p`.`prscdate` as date) between '2021-07-15' and now()) and (`d`.`qty` <> 0)) group by `p`.`prscdate`,`t`.`meditem` ;

-- ----------------------------
-- View structure for `report_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_visit` AS select date_format(`o`.`vstdttm`,'%M %Y') AS `m`,count((case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`vn` end)) AS `total`,count(distinct (case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`hn` end)) AS `hn` from `ovst` `o` where (date_format(`o`.`vstdttm`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`vstdttm`,'%M %Y') order by `o`.`vstdttm` ;

DROP VIEW IF EXISTS `report_admit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_admit` AS select date_format(`o`.`rgtdate`,'%Y-%m') AS `m`,count(`o`.`an`) AS `total`,count(`o`.`hn`) AS `hn` from `ipt` `o` where (date_format(`o`.`rgtdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`rgtdate`,'%M %Y') order by `o`.`rgtdate`;

DROP VIEW IF EXISTS `report_admit_dc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_admit_dc` AS select date_format(`o`.`dchdate`,'%M %Y') AS `m`,count(`o`.`an`) AS `total` from `ipt` `o` where (date_format(`o`.`dchdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`dchdate`,'%M %Y') order by `o`.`dchdate`;

DROP VIEW IF EXISTS `report_refer_er`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_refer_er` AS select date_format(`r`.`vstdate`,'%Y-%m') AS `m`,count(`r`.`rfrno`) AS `total`,count((case when ((`r`.`loads` between 1 and 3) and (`r`.`an` > 0)) then `r`.`rfrno` end)) AS `ambulance`,count((case when (`r`.`an` > 0) then `r`.`rfrno` end)) AS `ipd`,count((case when ((`r`.`an` = 0) and (`r`.`loads` not between 1 and 3)) then `r`.`rfrno` end)) AS `opd` from `orfro` `r` where (date_format(`r`.`vstdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`r`.`vstdate`,'%Y-%m') order by `r`.`vstdate`;
/*
Navicat MySQL Data Transfer

Source Server         : hi_master
Source Server Version : 50568
Source Host           : 192.168.111.5:3306
Source Database       : hi

Target Server Type    : MYSQL
Target Server Version : 50568
File Encoding         : 65001

Date: 2022-02-01 09:33:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for `report_er_dx`
-- ----------------------------
DROP VIEW IF EXISTS `report_er_dx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_er_dx` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`vn`) AS `total` from (((`ovst` `o` join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`o`.`an` = 0) and (`o`.`cln` = '20100') and (if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`o`.`vn`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_ip_dx`
-- ----------------------------
DROP VIEW IF EXISTS `report_ip_dx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_ip_dx` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`an`) AS `total` from (((`ipt` `o` join `iptdx` `x` on(((`o`.`an` = `x`.`an`) and (`x`.`itemno` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where (if((month(`o`.`dchdate`) < 10),year(`o`.`dchdate`),(year(`o`.`dchdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1))) group by `g`.`cgroup` order by count(`o`.`an`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_ip_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_ip_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_ip_visit` AS select if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) AS `yearbudget`,count(distinct `i`.`hn`) AS `people`,count(`i`.`an`) AS `admit`,sum(`i`.`daycnt`) AS `losd`,(timestampdiff(DAY,min(`i`.`dchdate`),max(`i`.`dchdate`)) + 1) AS `days`,((sum(`i`.`daycnt`) * 100) / (30 * (timestampdiff(DAY,min(`i`.`dchdate`),max(`i`.`dchdate`)) + 1))) AS `bed_rate` from `ipt` `i` group by if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) order by if((month(`i`.`dchdate`) < 10),(year(`i`.`dchdate`) + 543),(year(`i`.`dchdate`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_op_diag`
-- ----------------------------
DROP VIEW IF EXISTS `report_op_diag`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_op_diag` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`o`.`vn`) AS `total` from (((`ovst` `o` join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1) and (`x`.`icd10` not between 'V00' and 'Y9999') and (not((`x`.`icd10` like 'Z%')))))) join `icd101` `c` on((`x`.`icd10` = `c`.`icd10`))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`o`.`an` = 0) and (if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`o`.`vn`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_op_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_op_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_op_visit` AS select if((month(`o`.`vstdttm`) < 10),year(`o`.`vstdttm`),(year(`o`.`vstdttm`) + 1)) AS `yearbudget`,count(distinct (case when ((`o`.`dct` <> '') or (`o`.`cln` = '40100') or (`o`.`dct` is not null)) then `o`.`hn` end)) AS `people`,count(distinct (case when ((`o`.`dct` <> '') or (`o`.`cln` = '40100') or (`o`.`dct` is not null)) then `o`.`vn` end)) AS `visit`,count(distinct (case when (`o`.`cln` = '40100') then `o`.`vn` end)) AS `visit_dent`,count(distinct (case when (((`o`.`dct` <> '') or (`o`.`dct` is not null)) and (`o`.`cln` = '20100')) then `o`.`vn` end)) AS `visit_er`,count(distinct (case when (((`o`.`dct` <> '') or (`o`.`dct` is not null)) and (`o`.`cln` = '10100')) then `o`.`vn` end)) AS `visit_opd` from `ovst` `o` where (`o`.`an` = 0) group by if((month(`o`.`vstdttm`) < 10),(year(`o`.`vstdttm`) + 543),(year(`o`.`vstdttm`) + 544)) order by if((month(`o`.`vstdttm`) < 10),(year(`o`.`vstdttm`) + 543),(year(`o`.`vstdttm`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_refer`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_refer` AS select if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) AS `yearbudget`,count(`r`.`rfrno`) AS `total`,count((case when (`r`.`loads` between 1 and 3) then `r`.`rfrno` end)) AS `ambulance`,count((case when (`r`.`an` > 0) then `r`.`rfrno` end)) AS `ipd`,count((case when (`r`.`an` = 0) then `r`.`rfrno` end)) AS `opd` from `orfro` `r` group by if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) order by if((month(`r`.`vstdate`) < 10),(year(`r`.`vstdate`) + 543),(year(`r`.`vstdate`) + 544)) desc limit 6 ;

-- ----------------------------
-- View structure for `report_refer_ip`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer_ip`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_refer_ip` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`r`.`rfrno`) AS `total` from ((`orfro` `r` join `icd101` `c` on(((`r`.`icd10` = `c`.`icd10`) and (`r`.`icd10` not between 'V00' and 'Y9999') and (not((`r`.`icd10` like 'Z%')))))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`r`.`an` > 0) and (if((month(`r`.`vstdate`) < 10),year(`r`.`vstdate`),(year(`r`.`vstdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`r`.`rfrno`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_refer_op`
-- ----------------------------
DROP VIEW IF EXISTS `report_refer_op`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_refer_op` AS select `g`.`cgroup` AS `cgroup`,`g`.`e_name` AS `e_name`,`g`.`icd10` AS `icd10`,`g`.`t_name` AS `t_name`,count(`r`.`rfrno`) AS `total` from ((`orfro` `r` join `icd101` `c` on(((`r`.`icd10` = `c`.`icd10`) and (`r`.`icd10` not between 'V00' and 'Y9999') and (not((`r`.`icd10` like 'Z%')))))) join `cgroup298disease` `g` on(((convert(`c`.`group_298_disease` using utf8) = `g`.`cgroup`) and (`g`.`cgroup` <> 270)))) where ((`r`.`an` = 0) and (if((month(`r`.`vstdate`) < 10),year(`r`.`vstdate`),(year(`r`.`vstdate`) + 1)) = if((month(now()) < 10),year(now()),(year(now()) + 1)))) group by `g`.`cgroup` order by count(`r`.`rfrno`) desc limit 5 ;

-- ----------------------------
-- View structure for `report_visit`
-- ----------------------------
DROP VIEW IF EXISTS `report_visit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_visit` AS select date_format(`o`.`vstdttm`,'%M %Y') AS `m`,count((case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`vn` end)) AS `total`,count(distinct (case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`hn` end)) AS `hn` from `ovst` `o` where (date_format(`o`.`vstdttm`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`vstdttm`,'%M %Y') order by `o`.`vstdttm` ;


-- ----------------------------
-- Version 1.1 view 
-- ----------------------------
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `visit_pop` AS select (case when (timestampdiff(YEAR,`p`.`brthdate`,cast(`o`.`vstdttm` as date)) < 10) then '0-9' when (timestampdiff(YEAR,`p`.`brthdate`,cast(`o`.`vstdttm` as date)) between 10 and 19) then '10-19' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 20 and 29) then '20-29' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 30 and 39) then '30-39' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 40 and 49) then '40-49' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 50 and 59) then '50-59' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 60 and 69) then '60-69' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 70 and 79) then '70-79' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 80 and 89) then '80-89' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 90 and 99) then '90-99' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) >= 100) then '100+' end) AS `x`,count((case `p`.`male` when 1 then `o`.`hn` end)) AS `m`,count((case `p`.`male` when 2 then `o`.`hn` end)) AS `f` from (`ovst` `o` join `pt` `p` on((`o`.`hn` = `p`.`hn`))) where (cast(`o`.`vstdttm` as date) = cast(now() as date)) group by (case when (timestampdiff(YEAR,`p`.`brthdate`,cast(`o`.`vstdttm` as date)) < 10) then '0-9' when (timestampdiff(YEAR,`p`.`brthdate`,cast(`o`.`vstdttm` as date)) between 10 and 19) then '10-19' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 20 and 29) then '20-29' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 30 and 39) then '30-39' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 40 and 49) then '40-49' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 50 and 59) then '50-59' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 60 and 69) then '60-69' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 70 and 79) then '70-79' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 80 and 89) then '80-89' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) between 90 and 99) then '90-99' when (timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) >= 100) then '100+' end);

-- Update in Version 1.3
-- CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_admit` AS select date_format(`o`.`rgtdate`,'%M %Y') AS `m`,count(`o`.`an`) AS `total`,count(`o`.`hn`) AS `hn` from `ipt` `o` where (date_format(`o`.`rgtdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`rgtdate`,'%M %Y') order by `o`.`rgtdate`;

-- Update in Version 1.3
-- CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_admit_dc` AS select date_format(`o`.`dchdate`,'%M %Y') AS `m`,count(`o`.`an`) AS `total` from `ipt` `o` where (date_format(`o`.`dchdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`dchdate`,'%M %Y') order by `o`.`dchdate`;

-- ----------------------------
-- Version 1.2 add layout control
-- ----------------------------
insert into hi_setup (`common_code`,`value1`,`value2`) values ('dashboard','col-md-6','col-md-12');

-- ----------------------------
-- Version 1.3 update view
-- ----------------------------
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_refer_er` AS select date_format(`r`.`vstdate`,'%Y-%m') AS `m`,count(`r`.`rfrno`) AS `total`,count((case when ((`r`.`loads` between 1 and 3) and (`r`.`an` > 0)) then `r`.`rfrno` end)) AS `ambulance`,count((case when (`r`.`an` > 0) then `r`.`rfrno` end)) AS `ipd`,count((case when ((`r`.`an` = 0) and (`r`.`loads` not between 1 and 3)) then `r`.`rfrno` end)) AS `opd` from `orfro` `r` where (date_format(`r`.`vstdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`r`.`vstdate`,'%Y-%m') order by `r`.`vstdate`;

DROP VIEW IF EXISTS `report_visit`; 
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_visit` AS select date_format(`o`.`vstdttm`,'%Y-%m') AS `m`,count((case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`vn` end)) AS `total`,count(distinct (case when ((`o`.`cln` = 4) or (`o`.`dct` <> '')) then `o`.`hn` end)) AS `hn` from `ovst` `o` where (date_format(`o`.`vstdttm`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`vstdttm`,'%Y-%m') order by `o`.`vstdttm` ;

DROP VIEW IF EXISTS `report_admit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_admit` AS select date_format(`o`.`rgtdate`,'%Y-%m') AS `m`,count(`o`.`an`) AS `total`,count(`o`.`hn`) AS `hn` from `ipt` `o` where (date_format(`o`.`rgtdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`rgtdate`,'%Y-%m') order by `o`.`rgtdate`;

DROP VIEW IF EXISTS `report_admit_dc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report_admit_dc` AS select date_format(`o`.`dchdate`,'%Y-%m') AS `m`,count(`o`.`an`) AS `total` from `ipt` `o` where (date_format(`o`.`dchdate`,'%Y-%m') between date_format((now() - interval 1 year),'%Y-%m') and date_format(now(),'%Y-%m')) group by date_format(`o`.`dchdate`,'%Y-%m') order by `o`.`dchdate`;

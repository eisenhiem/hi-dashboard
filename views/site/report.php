<?php

use app\models\HiSetup;

$api_url = HiSetup::find()->where(['common_code' => 'API_PHR_SERVER'])->one()->value1;
// echo $api_url;
try {
    $json_data = file_get_contents($api_url . '/audit/patient-check');
    $result = json_decode($json_data);
    // echo $result->statusCode;
    if ($result->statusCode <> 200) {
        $connect = "ไม่พบข้อมูล";
    } else {
        $data = $result->results->rows[0];
        // var_dump($data);
    }
} catch (Exception $e) {
    $connect = $e;
}

$this->title = 'Report Audit';
?>
<div class="report">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">จำนวนเวชระเบียนผู้ป่วยทั้งหมด <?= $data->total ?> ราย</h3>
        </div>
        <div class="card-body">
            ความถูกต้องของเวชระเบียน
            <div class="row">
                <div class="col-2">
                    วันเกิด
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->check_brthdate) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_brthdate) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_brthdate) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_brthdate) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุวันเกิด <?= $data->check_brthdate ?> ราย
                </div>
                <div class="col-2">
                    เพศ
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_gender) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_gender) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_gender) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_gender) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุเพศ <?= $data->check_gender ?> ราย
                </div>
                <div class="col-2">
                    หมู่เลือด
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_blood) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_blood) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_blood) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_blood) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุหมู่เลือด <?= $data->check_blood ?> ราย
                </div>
                <div class="col-2">
                    ประวัติแพ้ยา
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_allergy) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_allergy) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_allergy) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_allergy) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุประวัติแพ้ยา <?= $data->check_allergy ?> ราย
                </div>
                <div class="col-2">
                    ที่อยู่
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_address) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_address) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_address) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_address) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุบ้านเลขที่ <?= $data->check_address ?> ราย
                </div>
                <div class="col-2">
                    หมู่บ้าน
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_village) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_village) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_village) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_village) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุหมู่บ้าน <?= $data->check_village ?> ราย
                </div>
                <div class="col-2">
                    ตำบล
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_tambon) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_tambon) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_tambon) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_tambon) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุตำบล <?= $data->check_tambon ?> ราย
                </div>
                <div class="col-2">
                    อำเภอ
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_amphur) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_amphur) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_amphur) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_amphur) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุอำเภอ <?= $data->check_amphur ?> ราย
                </div>
                <div class="col-2">
                    จังหวัด
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_changwat) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_changwat) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_changwat) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_changwat) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุจังหวัด <?= $data->check_changwat ?> ราย
                </div>
                <div class="col-2">
                    เบอร์โทรศัพท์
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_phone_no) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_phone_no) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_phone_no) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_phone_no) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุเบอร์โทรศัพท์ <?= $data->check_phone_no ?> ราย
                </div>
                <div class="col-2">
                    สถานะภาพสมรส
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_married_status) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_married_status) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_married_status) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_married_status) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุสถานะภาพสมรส <?= $data->check_married_status ?> ราย
                </div>
                <div class="col-2">
                    มารดา
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_mother_name) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_mother_name) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_mother_name) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_mother_name) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุมารดา <?= $data->check_mother_name ?> ราย
                </div>
                <div class="col-2">
                    บิดา
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_father_name) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_father_name) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_father_name) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_father_name) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุบิดา <?= $data->check_father_name ?> ราย
                </div>
                <div class="col-2">
                    ผู้ติดต่อได้
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_inform_name) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_inform_name) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_inform_name) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_inform_name) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุผู้ติดต่อได้ <?= $data->check_inform_name ?> ราย
                </div>
                <div class="col-2">
                    ที่อยู่ผู้ติดต่อได้
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_inform_address) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_inform_address) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_inform_address) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_inform_address) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุที่อยู่ผู้ติดต่อได้ <?= $data->check_inform_address ?> ราย
                </div>
                <div class="col-2">
                    เบอร์โทรผู้ติดต่อได้
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_inform_phone_no) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_inform_phone_no) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_inform_phone_no) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_inform_phone_no) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุเบอร์โทรผู้ติดต่อได้ <?= $data->check_inform_phone_no ?> ราย
                </div>
                <div class="col-2">
                    ความสัมพันธ์กับผู้มารับบริการ
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_inform_relation) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_inform_relation) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_inform_relation) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_inform_relation) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ได้ระบุความสัมพันธ์กับผู้มารับบริการ <?= $data->check_inform_relation ?> ราย
                </div>
                <div class="col-2">
                    เลขบัตรประชาชน
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar
                        <?php
                        if (number_format(($data->total - $data->check_cid) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->check_cid) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->check_cid) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->check_cid) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    คนไทยที่เลขบัตรประชาชนไม่ถูกต้อง <?= $data->check_cid ?> ราย
                </div>
            </div>
        </div>
    </div>

</div>
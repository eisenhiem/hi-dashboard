<?php

use app\models\ReportIpVisit;
use miloschuman\highcharts\Highcharts;


$graph_ip = ReportIpVisit::find()->orderBy(['yearbudget'=>SORT_ASC])->limit(5)->all();
foreach($graph_ip as $g){
    $x[] = $g->yearbudget;
    $y1[] = $g->admit*1; 
    $y2[] = $g->people*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'IP Admit 5 ปี'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'admit',
                'data' => $y1,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

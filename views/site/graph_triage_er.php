<?php

use app\models\Ovst;
use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;


// $graph_op = Ovst::find()->joinWith('triage')->select('triage as x,count(ovst.vn) as total')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100'])->groupBy('triage')->orderBy('triage')->all();
$y0 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>''])->count();
$y1 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>'1'])->count();
$y2 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>'2'])->count();
$y3 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>'3'])->count();
$y4 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>'4'])->count();
$y5 = Ovst::find()->joinWith('triage')->where('date(vstdttm) = date(now())')->andWhere(['cln' => '20100','triage'=>'5'])->count();

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนผู้มารับบริการ ER แยกตาม Triage'],
        'xAxis' => [
            'categories' => ['Triage ER'],
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'Resuscitate',
                'data' => [$y1*1],
                'color' => '#EB1D36',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'Emergency',
                'data' => [$y2*1],
                'color' => '#E8AA42',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'Urgency',
                'data' => [$y3*1],
                'color' => '#F4E06D',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'Semi Urgency',
                'data' => [$y4*1],
                'color' => '#65C18C',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'Non Urgent',
                'data' => [$y5*1],
                'color' => 'white',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'ไม่ระบุ',
                'data' => [$y0*1],
                'color' => 'grey',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
/*            
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
            ],
*/
        ],
    ]
]);

?>

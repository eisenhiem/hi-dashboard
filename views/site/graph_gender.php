<?php

use app\models\Ovst;
use app\models\ReportVisit;
use app\models\VisitPop;
use miloschuman\highcharts\Highcharts;


$graph_op = VisitPop::find()->all();
foreach ($graph_op as $g) {
    $x[] = $g->x;
    $y1[] = 0 - ($g->m * 1);
    $y2[] = $g->f * 1;
    //    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'ผู้มารับบริการ แยกเพศและช่วงอายุ'],
        'xAxis' => [
            [
                'categories' => $x,
                'reversed' => false,
                'labels' => [
                    'step' => 1,
                ],
                'accessibility' => [
                    'description' => 'Male',
                ],
            ],
            [
                'categories' => $x,
                'opposite' => true,
                'reversed' => false,
                'linkedTo' => 0,
                'labels' => [
                    'step' => 1,
                ],
                'accessibility' => [
                    'description' => 'Female',
                ],
            ],
        ],
        'yAxis' => [
            'title' => ['text' => '']
        ],
        'plotOptions' => [
            'series' => [
                'stacking' => 'normal',
            ]
        ],
        'series' => [
            [
                'type' => 'bar',
                'name' => 'ชาย',
                'data' => $y1,
                'color' => '#92B4EC',
            ],
            [
                'type' => 'bar',
                'name' => 'หญิง',
                'data' => $y2,
                'color' => '#FF7396',
            ],
            /*            
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
            ],
*/
        ],
    ]
]);

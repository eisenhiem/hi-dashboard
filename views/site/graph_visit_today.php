<?php

use app\models\Ovst;
use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = Ovst::find()->select('cln,count(vn) as total')->where('date(vstdttm) = date(now())')->groupBy('cln')->all();
foreach($graph_op as $g){
    if($g->cln == ''){
        $g->x = 'ไม่ระบุ';
    } else {
        $g->x = $g->clinic->namecln;
    }
    $x[] = $g->x;
    $y1[] = $g->total*1; 
//    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนผู้มารับบริการแยก Clinic'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ครั้ง',
                'data' => $y1,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
/*            
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
            ],
*/
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                    'inside' => false,
                ]
            ],
        ],

    ]
]);

?>

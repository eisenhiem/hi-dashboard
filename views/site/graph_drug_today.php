<?php

use app\models\Prsc;

use miloschuman\highcharts\Highcharts;


$graph_op = Prsc::find()->joinWith('prscdt')->select('meditem as x,sum(qty) as total,sum(prscdt.charge) as y2')->where('date(prscdate) = date(now())')->groupBy('meditem')->orderBy(['total'=>SORT_DESC])->limit(10)->all();
foreach($graph_op as $g){
    $x[] = $g->getMedname($g->x);
    $y1[] = $g->total*1; 
    $y2[] = $g->y2*1;
//    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนการใช้ยา 10 อันดับแรก วันนี้'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $y1,
                'color' => '#40DFA0',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],            
            [
                'type' => 'column',
                'name' => 'ราคา',
                'data' => $y2,
                'color' => '#F0AFA0',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

<?php

use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportVisit::find()->all();
foreach($graph_op as $g){
    $x[] = ReportVisit::getVisitdate($g->m.'-01','month');
    $y1[] = $g->total*1; 
    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนผู้มารับบริการผู้ป่วยนอกย้อนหลัง 12 เดือน'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ครั้ง',
                'data' => $y1,
                'color' => '#6ECB63',
            ],
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
                'color' => '#B1E693',
            ],
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                    'inside' => false,
                ]
            ],
        ],
    ],    
]);

?>

<?php

use app\models\ReportOpVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportOpVisit::find()->orderBy(['yearbudget'=>SORT_ASC])->limit(5)->all();
foreach($graph_op as $g){
    $x[] = $g->yearbudget;
    $y[] = round($g->visit*1/365,0); 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'OP Visit เฉลี่ย/วัน'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน/วัน'],
        ],
        'series' => [
            [
                'type' => 'line',
                'name' => 'ครั้ง/วัน',
                'data' => $y,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard IPD';
?>

<div class="row">
    <div class="col-md-6">
        <?= $this->render('graph_ip') ?>
    </div>
    <div class="col-md-6">
        <?= $this->render('graph_ip_rate') ?>
    </div>
    <div class="col-md-12">
        <?= $this->render('graph_ip_diag') ?>
    </div>
</div>
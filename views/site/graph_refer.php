<?php

use app\models\ReportRefer;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportRefer::find()->orderBy(['yearbudget'=>SORT_ASC])->limit(5)->all();
foreach($graph_op as $g){
    $x[] = $g->yearbudget;
    $y1[] = $g->total*1; 
    $y2[] = $g->ipd*1; 
    $y3[] = $g->opd*1;
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'Refer 5 ปี'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'Total Refer',
                'data' => $y1,
            ],
            [
                'type' => 'column',
                'name' => 'IPD Refer',
                'data' => $y2,
            ],
            [
                'type' => 'column',
                'name' => 'OPD Refer',
                'data' => $y3,
            ],
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                    'inside' => false,
                ]
            ],
        ],
    ]
]);

?>

<?php

use app\models\HiSetup;

$api_url = HiSetup::find()->where(['common_code' => 'API_PHR_SERVER'])->one()->value1;

try {
    $json_data = file_get_contents($api_url . '/audit/external-clause');
    $result = json_decode($json_data);
    // echo $result->statusCode;
    if ($result->statusCode <> 200) {
        $connect = "ไม่พบข้อมูล";
    } else {
        $data2 = $result->results->rows[0];
        // var_dump($data);
    }
} catch (Exception $e) {
    $connect = $e;
}

try {
    $json_data = file_get_contents($api_url . '/audit/vital-sign');
    $result = json_decode($json_data);
    // echo $result->statusCode;
    if ($result->statusCode <> 200) {
        $connect = "ไม่พบข้อมูล";
    } else {
        $data = $result->results->rows[0];
        // var_dump($data);
    }
} catch (Exception $e) {
    $connect = $e;
}

$this->title = 'Report Audit Screening and Diagnosis';
?>
<div class="report">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">จำนวนผู้มารับบริการทั้งหมด <?= $data->total ?> ราย</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    น้ำหนัก
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_bw) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_bw) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_bw) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_bw) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุน้ำหนัก <?= $data->miss_bw ?> ราย
                </div>
                <div class="col-2">
                    อุณหภูมิร่างกาย
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_bt) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_bt) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_bt) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_bt) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุอุณหภูมิร่างกาย <?= $data->miss_bt ?> ราย
                </div>
                <div class="col-2">
                    ความดันโลหิต
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_sbp) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_sbp) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_sbp) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_sbp) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุความดันโลหิต <?= $data->miss_sbp ?> ราย
                </div>
                <div class="col-2">
                    อัตราการเต้นหัวใจ
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_pr) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_pr) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_pr) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_pr) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุอัตราการเต้นหัวใจ <?= $data->miss_pr ?> ราย
                </div>
                <div class="col-2">
                    อัตราการหายใจ
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_rr) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_rr) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_rr) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_rr) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุอัตราการหายใจ <?= $data->miss_rr ?> ราย
                </div>
                <div class="col-2">
                    รอบเอว
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_waist) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_waist) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_waist) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_waist) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุรอบเอว <?= $data->miss_waist ?> ราย
                </div>
                <div class="col-2">
                    แพทย์
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_doctor) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_doctor) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_doctor) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_doctor) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุแพทย์ตรวจ/ไม่ได้พบแพทย์ <?= $data->miss_doctor ?> ราย
                </div>
                <div class="col-2">
                    สถานะจำหน่าย
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data->total - $data->miss_out_status) * 100 / $data->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data->total - $data->miss_out_status) * 100 / $data->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data->total - $data->miss_out_status) * 100 / $data->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data->total - $data->miss_out_status) * 100 / $data->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุสถานะจำหน่าย <?= $data->miss_out_status ?> ราย
                </div>
                <div class="col-2">
                    การวินิจฉัยโรค
                </div>
                <div class="col-10">
                    <div class="progress">
                        <div class="progress-bar                     
                        <?php
                        if (number_format(($data2->total - $data2->miss_ex) * 100 / $data2->total, 2) > 80) {
                            echo 'bg-success';
                        } else {
                            if (number_format(($data2->total - $data2->miss_ex) * 100 / $data2->total, 2) > 50) {
                                echo 'bg-warning';
                            } else {
                                echo 'bg-danger';
                            }
                        } ?>" role="progressbar" style="width: <?= number_format(($data2->total - $data2->miss_ex) * 100 / $data2->total, 2) ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= number_format(($data2->total - $data2->miss_ex) * 100 / $data2->total, 2) ?>%</div>
                    </div>
                    ไม่ระบุสาเหตุภายนอก <?= $data2->miss_ex ?> ราย
                </div>
            </div>
        </div>
    </div>
</div>
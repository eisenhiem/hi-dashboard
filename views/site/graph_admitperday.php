<?php

use app\models\Incoth;
use app\models\Ipt;
use miloschuman\highcharts\Highcharts;


/* @var $this yii\web\View */

$cost = Ipt::find()
->joinWith('type')
->select("rgtdate,count(case pttype.inscl when 'UCS' then an end) as y1,count(case pttype.inscl when 'OFC' then an end) as y2,count(case pttype.inscl when 'LGO' then an end) as y3,count(case pttype.inscl when 'SSS' then vn end) as y4,count(case when (pttype.inscl = 'AAA' and pttype.pttype <> '35') then an end) as y5,count(case when (pttype.inscl = 'AAA' and pttype.pttype = '35') then an end) as y6")
->where(['year(rgtdate)' => date('Y')])
->andWhere(['month(rgtdate)' => date('m')])
->andWhere(['<>','an',0])
->groupBy('rgtdate')
->all();

foreach($cost as $v){
    $x [] = Incoth::getVisitDate($v->rgtdate,'day');
    $y1 [] = $v->y1*1;
    $y2 [] = $v->y2*1;
    $y3 [] = $v->y3*1;
    $y4 [] = $v->y4*1;
    $y5 [] = $v->y5*1;
    $y6 [] = $v->y6*1;
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'ผู้ป่วย Admit ตามประเภทสิทธิ์'],
        'xAxis' => [
            'categories' => $x
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวนผู้ป่วย(ราย)']
        ],
/*
        'plotOptions' => [
            'column' => [
                'stacking' => 'normal'
                
            ],            
        ],
 */   
        'series' => [
            [
                'type' => 'column',
                'name' => 'ประกันสุขภาพ',
                'data' => $y1
            ],
            [
                'type' => 'column',
                'name' => 'ข้าราชการ',
                'data' => $y2
            ],
            [
                'type' => 'column',
                'name' => 'อปท.',
                'data' => $y3
            ],
            [
                'type' => 'column',
                'name' => 'ประกันสังคม',
                'data' => $y4
            ],
            [
                'type' => 'column',
                'name' => 'พรบ.จราจร',
                'color' => 'red',
                'data' => $y6
            ],
            [
                'type' => 'column',
                'name' => 'ชำระเงิน',
                'data' => $y5
            ],
        ],
    ]
]);
?>

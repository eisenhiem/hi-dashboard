<?php

use app\models\Incoth;
use miloschuman\highcharts\Highcharts;


/* @var $this yii\web\View */

$cost = Incoth::find()
->joinWith('pttype')
->select("date,sum(case pttype.inscl when 'UCS' then rcptamt end) as y1,sum(case pttype.inscl when 'OFC' then rcptamt end) as y2,sum(case pttype.inscl when 'LGO' then rcptamt end) as y3,sum(case pttype.inscl when 'SSS' then rcptamt end) as y4,sum(case when (pttype.inscl = 'AAA' and pttype.pttype <> '35') then rcptamt end) as y5,sum(case when (pttype.inscl = 'AAA' and pttype.pttype = '35') then rcptamt end) as y6")
->where(['year(date)' => date('Y')])
->andWhere(['month(date)' => date('m')])
->groupBy('date')
->all();

foreach($cost as $v){
    $x [] = Incoth::getVisitDate($v->date,'day');
    $y1 [] = $v->y1*1;
    $y2 [] = $v->y2*1;
    $y3 [] = $v->y3*1;
    $y4 [] = $v->y4*1;
    $y5 [] = $v->y5*1;
    $y6 [] = $v->y6*1;
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'ลูกหนี้แยกตามประเภทสิทธิ์'],
        'xAxis' => [
            'categories' => $x
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวนเงิน(บาท)']
        ],
/*
        'plotOptions' => [
            'column' => [
                'stacking' => 'normal'
                
            ],            
        ],
 */   
        'series' => [
            [
                'type' => 'column',
                'name' => 'ประกันสุขภาพ',
                'data' => $y1
            ],
            [
                'type' => 'column',
                'name' => 'ข้าราชการ',
                'data' => $y2
            ],
            [
                'type' => 'column',
                'name' => 'อปท.',
                'data' => $y3
            ],
            [
                'type' => 'column',
                'name' => 'ประกันสังคม',
                'data' => $y4
            ],
            [
                'type' => 'column',
                'name' => 'พรบ.จราจร',
                'color' => 'red',
                'data' => $y6
            ],
            [
                'type' => 'column',
                'name' => 'ชำระเงิน',
                'data' => $y5
            ],
        ],
    ]
]);
?>

<?php

use app\models\ReportAdmit;
use app\models\ReportAdmitDc;
use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;


$graph_admit = ReportAdmit::find()->all();
$graph_dc = ReportAdmitDc::find()->all();

foreach($graph_admit as $g){
    $x[] = ReportVisit::getVisitdate($g->m.'-01','month');
    $y1[] = $g->total*1; 
}
foreach($graph_dc as $g){
    $y2[] = $g->total*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนการให้บริการผู้ป่วยในย้อนหลัง 12 เดือน'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'Admit',
                'data' => $y1,
                'color' => '#FF6363',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'DC',
                'data' => $y2,
                'color' => '#A0D995',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

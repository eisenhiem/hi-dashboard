<?php

use app\models\ReportIpVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportIpVisit::find()->orderBy(['yearbudget'=>SORT_ASC])->limit(5)->all();
foreach($graph_op as $g){
    $x[] = $g->yearbudget;
    $y[] = $g->bed_rate*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'อัตราการครองเตียง'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => '%'],
        ],
        'series' => [
            [
                'type' => 'line',
                'name' => 'bed rate',
                'data' => $y,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

<?php

use app\models\ReportReferEr;
use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;

$graph_er = ReportReferEr::find()->all();

foreach($graph_er as $g){
    $x[] = ReportVisit::getVisitdate($g->m.'-01','month');
    $y0[] = $g->total*1; 
    $y1[] = $g->opd*1; 
    $y2[] = $g->ipd*1; 
    $y3[] = $g->ambulance*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนการส่งต่อผู้ป่วยย้อนหลัง 12 เดือน'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ผู้ป่วยนอก',
                'data' => $y1,
                'color' => '#95CD41',
            ],
            [
                'type' => 'column',
                'name' => 'ส่งต่อผู้ป่วยฉุกเฉิน',
                'data' => $y3,
                'color' => '#FF7F3F',
            ],
            [
                'type' => 'column',
                'name' => 'ผู้ป่วยใน',
                'data' => $y2,
                'color' => '#F6D860',
            ],
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    'enabled' => true,
                    'inside' => false,
                ]
            ],
        ],
    ]
]);

?>

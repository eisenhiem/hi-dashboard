<?php

use app\models\Ipt;
use miloschuman\highcharts\Highcharts;


$graph_op = Ipt::find()->select('ward,count(an) as total')->where(['dchdate' => '0000-00-00'])->groupBy('ward')->all();
foreach($graph_op as $g){
    if($g->ward == ''){
        $g->x = 'ไม่ระบุ';
    } else {
        $g->x = $g->wardadmit->nameidpm;
    }
    $x[] = $g->x;
    $y1[] = $g->total*1; 
//    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'จำนวนผู้ป่วยที่ยัง Admit'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ราย',
                'data' => $y1,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
/*            
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
            ],
*/
        ],
    ]
]);

?>

<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard OPD';
?>

<div class="row">
    <div class="col-md-6">
        <?= $this->render('graph_op') ?>
    </div>
    <div class="col-md-6">
        <?= $this->render('graph_op_rate') ?>
    </div>
    <div class="col-md-12">
        <?= $this->render('graph_op_diag') ?>
    </div>
</div>
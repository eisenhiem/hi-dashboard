<?php

use app\models\Lbbk;
use app\models\Ovst;
use app\models\ReportVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = Lbbk::find()->joinWith('lab')->select('lbbk.labcode,sum(lab.pricelab) as total,count(ln) as y2')->where('date(vstdttm) = date(now())')->andWhere(['accept' => '1'])->groupBy('lbbk.labcode')->orderBy(['total' => SORT_DESC])->limit(10)->all();
foreach($graph_op as $g){

    $x[] = $g->lab->labname;
    $y1[] = $g->total*1; 
    $y2[] = $g->y2*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'ค่าใช้จ่าย LAB 10 อันดับแรก วันนี้'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            [ // primary axis
                'title' => ['text' => 'บาท'],
            ],
            [ // secondary axis
                'gridLineWidth' => 0,
                'title' => ['text' => 'test'],
                'opposite' => true,
            ]
        ],
        'series' => [
            [
                'type' => 'column',
                'yAxis' => 0,
                'name' => 'ค่าใช้จ่าย(บาท)',
                'data' => $y1,
                'color' => '#EB4747',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'yAxis' => 1,
                'name' => 'จำนวน(test)',
                'data' => $y2,
                'color' => '#ABC9FF',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

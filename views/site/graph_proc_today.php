<?php

use app\models\Oprt;
use app\models\Prsc;

use miloschuman\highcharts\Highcharts;


$graph_op = Oprt::find()->select('icd9name as x,sum(charge) as total,count(id) as y2')->where('date(opdttm) = date(now())')->groupBy('icd9name')->orderBy(['total'=>SORT_DESC])->limit(10)->all();
foreach($graph_op as $g){
    $x[] = $g->x;
    $y1[] = $g->total*1; 
    $y2[] = $g->y2*1;
//    $y2[] = $g->hn*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'ค่าใช้จ่ายการทำหัตถการ 10 อันดับแรก วันนี้'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            [ // primary axis
                'title' => ['text' => 'บาท'],
            ],
            [ // secondary axis
                'gridLineWidth' => 0,
                'title' => ['text' => 'ครั้ง'],
                'opposite' => true,
            ]
        ],
        'series' => [
            [
                'type' => 'column',
                'yAxis' => 0,
                'name' => 'ค่าใช้จ่าย(บาท)',
                'data' => $y1,
                'color' => '#EB4747',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'yAxis' => 1,
                'name' => 'จำนวน(ครั้ง)',
                'data' => $y2,
                'color' => '#ABC9FF',
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

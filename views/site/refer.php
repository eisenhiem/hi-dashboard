<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard REFER';
?>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('graph_refer') ?>
    </div>
    <div class="col-md-12">
        <?= $this->render('graph_op_refer_diag') ?>
    </div>
    <div class="col-md-12">
        <?= $this->render('graph_ip_refer_diag') ?>
    </div>
</div>
<?php

/* @var $this yii\web\View */

use app\models\ReportIpVisit;
use app\models\ReportOpVisit;
use app\models\ReportRefer;
use deyraka\materialdashboard\widgets\CardStats;
use deyraka\materialdashboard\widgets\CardProduct;
use deyraka\materialdashboard\widgets\CardChart;
use deyraka\materialdashboard\widgets\Card;
use deyraka\materialdashboard\widgets\Progress;
use yii\helpers\Url;

$current_visit = ReportOpVisit::find()->orderBy(['yearbudget' => SORT_DESC])->one();
$current_admit = ReportIpVisit::find()->orderBy(['yearbudget' => SORT_DESC])->one();
$current_refer = ReportRefer::find()->orderBy(['yearbudget' => SORT_DESC])->one();

$date = date_create(($current_admit->yearbudget*1-544).'-10-01');
$cur_date = date_create(date('Y-m-d'));
$diff = date_diff($date,$cur_date); 

$this->title = 'Dashboard';
?>
<div class="site-index">
    <div class="body-content">
        <h2> ข้อมูลปีงบประมาณ ปัจจุบัน </h2>
        <div class="row text-right">
            <div class="col-md-4">
                <?php
                Card::begin([
                    'id' => 'card1',
                    'color' => Card::COLOR_INFO,
                    'headerIcon' => 'medical_services',
                    'collapsable' => false,
                    'title' => 'OP Visit',
                    'titleTextType' => Card::TYPE_INFO,
                    'showFooter' => false,
                    'footerContent' => 'Report Date ' . date('d M Y'),
                ])
                ?>
                <!-- START your <body> content of the Card below this line  -->
                <span class='col-md-12'>
                    <h2><?= number_format($current_visit->visit, 0) ?> ครั้ง</h2>
                    <h3><?= number_format($current_visit->people, 0) ?> คน</h3>
                    <h5>อัตรารับบริการต่อวัน <?= number_format($current_visit->visit/ $diff->format('%a'),2) ?></h5>
                    <h5>อัตรารับบริการ ER ต่อวัน <?= number_format($current_visit->visit_er/ $diff->format('%a'),2) ?></h5>
                </span>
                <!-- END your <body> content of the Card above this line, right before "Card::end()"  -->
                <?php Card::end(); ?>
            </div>
            <div class="col-md-4">
                <?php
                Card::begin([
                    'id' => 'card1',
                    'color' => Card::COLOR_SUCCESS,
                    'headerIcon' => 'hotel',
                    'collapsable' => false,
                    'title' => 'IP Visit',
                    'titleTextType' => Card::TYPE_SUCCESS,
                    'showFooter' => false,
                    'footerContent' => 'Report Date ' . date('d M Y'),
                ])
                ?>
                <!-- START your <body> content of the Card below this line  -->
                <span class='col-md-6'>
                    <h2><?= number_format($current_admit->admit, 0) ?> ครั้ง</h2>
                    <h3><?= number_format($current_admit->losd, 0) ?> วันนอน</h3>
                    <h5>อัตราการครองเตียง <?= number_format($current_admit->losd*100/(30*$diff->format('%a')),2) ?> %</h5>
                    <h5>Active Bed <?= number_format($current_admit->losd/($diff->format('%a')),2) ?></h5>
                </span>
                <!-- END your <body> content of the Card above this line, right before "Card::end()"  -->
                <?php Card::end(); ?>
            </div>
            <div class="col-md-4">
                <?php
                Card::begin([
                    'id' => 'card1',
                    'color' => Card::COLOR_DANGER,
                    'headerIcon' => 'local_shipping',
                    'collapsable' => false,
                    'title' => 'Refer',
                    'titleTextType' => Card::TYPE_DANGER,
                    'showFooter' => false,
                    'footerContent' => 'Report Date ' . date('d M Y'),
                ])
                ?>
                <!-- START your <body> content of the Card below this line  -->
                <span class='col-md-6'>
                    <h2><?= number_format($current_refer->total, 0) ?> ครั้ง</h2>
                    <h3>Ambulance <?= number_format($current_refer->ambulance, 0) ?> ครั้ง</h3>
                    <h5>อัตราการ Refer ต่อวัน <?= number_format($current_refer->total/($diff->format('%a')),2) ?></h5>
                    <h5>อัตราการ Refer ด้วย Ambulance <?= number_format($current_refer->ambulance/($diff->format('%a')),2) ?></h5>
                </span>

                <!-- END your <body> content of the Card above this line, right before "Card::end()"  -->
                <?php Card::end(); ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->render('graph_visit') ?>
        </div>
        <div class="col-md-12">
            <?= $this->render('graph_admit_12m') ?>
        </div>
        <div class="col-md-12">
            <?= $this->render('graph_refer_12m') ?>
        </div>
    </div>
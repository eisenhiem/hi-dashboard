<?php

use app\models\ReportOpVisit;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportOpVisit::find()->orderBy(['yearbudget'=>SORT_ASC])->limit(5)->all();
foreach($graph_op as $g){
    $x[] = $g->yearbudget;
    $y1[] = $g->visit*1; 
    $y2[] = $g->people*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'OP Visit 5 ปี'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ครั้ง',
                'data' => $y1,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
            [
                'type' => 'column',
                'name' => 'คน',
                'data' => $y2,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

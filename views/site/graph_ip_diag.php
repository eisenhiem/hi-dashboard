<?php


use app\models\ReportIpDx;
use miloschuman\highcharts\Highcharts;


$graph_op = ReportIpDx::find()->all();
foreach($graph_op as $g){
    $x[] = $g->t_name;
    $y[] = $g->total*1; 
}

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting', 
        'themes/grid-light'
    ],
    'options' => [
        'title' => ['text' => 'TOP 5 IP Diagnosis'],
        'xAxis' => [
            'categories' => $x,
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน(ราย)'],
        ],
        'series' => [
            [
                'type' => 'bar',
                'name' => 'Diagnosis',
                'data' => $y,
                'dataLabels' => [
                    'enabled' =>true,
                ],
            ],
        ],
    ]
]);

?>

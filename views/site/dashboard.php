<?php

/* @var $this yii\web\View */

use app\models\HiSetup;
use app\models\Incoth;
use yii\bootstrap4\Html;

$graph = HiSetup::find()->where(['common_code' => 'dashboard'])->one();
$d = Incoth::getvisitDate(date('Y-m-d'),'short');
$this->title = 'Dashboard Realtime วันที่ '. $d;
?>
<div class="site-dashboard">
<?= Html::a('ย่อ/ขยาย', ['change'], ['class' => 'btn btn-primary']) ?>

    <div class="body-content">
        <div class="row text-right">
            <div class=<?= $graph->value1 ?>>
            <?= $this->render('graph_visit_today') ?>
            </div>
            <div class=<?= $graph->value1 ?>>
            <?= $this->render('graph_triage_er') ?>
            </div>
            <div class=<?= $graph->value1 ?>>
            <?= $this->render('graph_admit_today') ?>
            </div>
            <div class=<?= $graph->value1 ?>>
            <?= $this->render('graph_admit') ?>
            </div>
            <div class="col-md-12">
            <?= $this->render('graph_drug_today') ?>
            </div>
            <div class="col-md-12">
            <?= $this->render('graph_proc_today') ?>
            </div>
            <div class="col-md-12">
            <?= $this->render('graph_lab_today') ?>
            </div>
            <div class="col-md-6">
            <?= $this->render('graph_opd_cost') ?>
            </div>
            <div class="col-md-6">
            <?= $this->render('graph_ipd_cost') ?>
            </div>
            <div class="col-md-6">
            <?= $this->render('graph_gender') ?>
            </div>
            <div class="col-md-6">
            <?= $this->render('graph_visit_pttype') ?>
            </div>
        </div>
    </div>
</div>
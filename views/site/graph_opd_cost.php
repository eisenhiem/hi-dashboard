
<?php

use app\models\Incoth;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */

$opd = Incoth::find()
->joinWith('pttype')
->joinWith('ovst')
->select("(case when pttype.inscl = 'UCS' then 'ประกันสุขภาพ' when pttype.inscl = 'SSS' then 'ประกันสังคม' when pttype.inscl = 'OFC' then 'ข้าราชการ' when pttype.inscl = 'LGO' then 'อปท.' when (pttype.inscl = 'AAA' and ovst.pttype = '35') then 'พรบ.จราจร' when (pttype.inscl = 'AAA' and ovst.pttype <> '35') then 'ชำระเงิน' end) as x,sum(rcptamt) as y1")
->where(['date(date)' => date('Y-m-d')])
->andWhere(['ovst.an'=>0])
->groupBy('x')
->orderBy(['y1' => SORT_DESC])
->all();

$data = [];

// push data to array
foreach($opd as $d){
//    echo $d->x.':'.$d->y1.'<br>'; // print value to view data get
    array_push($data,[$d->x,$d->y1*1]);
}

// export view data
// var_dump($data);

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'ค่าใช้จ่าย OPD แยกตามสิทธิ์ วันนี้'],
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
                'innerSize' => '50%',
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => 'ค่าใช้จ่าย',
                'data' => $data,
            ] // new closing bracket
        ],
    ],
]);
?>

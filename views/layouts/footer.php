<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="https://gitlab.com/eisenhiem/hi-dashboard.git">
                        Jakkrapong Wongkamalasai
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy; <?=date('Y');?> <i class="material-icons">favorite</i> by
            <a href="https://gitlab.com/eisenhiem/hi-dashboard.git" target="_blank">jwongkamalasai@gmail.com</a> for material UI.
        </div>
    </div>
</footer>

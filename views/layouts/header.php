<?php
use yii\helpers\Html;
?>
<nav class="navbar navbar-transparent navbar-expand-lg navbar-absolute fixed-top" role="navigation-demo">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#"><?=$this->title;?></a>
        </div>

    </div>
    <!-- /.container-->
</nav>
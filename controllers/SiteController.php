<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\HiSetup;
use app\models\ReportIpVisit;
use app\models\ReportOpVisit;
use app\models\ReportRefer;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionDashboard()
    {
        return $this->render('dashboard');
    }
    public function actionOpd()
    {
        return $this->render('opd');
    }
    public function actionIpd()
    {
        return $this->render('ipd');
    }
    public function actionRefer()
    {
        return $this->render('refer');
    }

    public function actionReport()
    {
        return $this->render('report');
    }

    public function actionExamination()
    {
        return $this->render('examination');
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionChange()
    {
        $model = HiSetup::find()->where(['common_code' => 'dashboard'])->one();
        if ($model->value1 == 'col-md-6') {
            $model->value1 = 'col-md-12';
        } else {
            $model->value1 = 'col-md-6';
        }

        $model->save();

        return $this->redirect(['dashboard']);
    }
}

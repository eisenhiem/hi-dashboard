<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visit_pop".
 *
 * @property string|null $x
 * @property int $m
 * @property int $f
 */
class VisitPop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visit_pop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['m', 'f'], 'integer'],
            [['x'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'x' => 'X',
            'm' => 'M',
            'f' => 'F',
        ];
    }
}

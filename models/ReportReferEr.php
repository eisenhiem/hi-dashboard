<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_refer_er".
 *
 * @property int|null $yearbudget
 * @property int $total
 * @property int $ambulance
 * @property int $ipd
 * @property int $opd
 */
class ReportReferEr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_refer_er';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['m', 'total', 'ambulance', 'ipd', 'opd'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'm' => 'Month',
            'total' => 'Total',
            'ambulance' => 'Ambulance',
            'ipd' => 'Ipd',
            'opd' => 'Opd',
        ];
    }
}

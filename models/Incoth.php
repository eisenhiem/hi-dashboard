<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "incoth".
 *
 * @property int $vn
 * @property int $an
 * @property string $date
 * @property int $time
 * @property string $income
 * @property string $pttype
 * @property string $paidst
 * @property int $rcptno
 * @property float $rcptamt
 * @property int $recno
 * @property string $cgd
 * @property string $codecheck
 * @property int $ln
 * @property int $id
 */
class Incoth extends \yii\db\ActiveRecord
{
    public $total;
    public $x;
    public $y1;
    public $y2;
    public $y3;
    public $y4;
    public $y5;
    public $y6;
    public $y7;
    public $y8;
    public $y9;
    public $y10;
    public $y11;
    public $y12;
    public $y13;
    public $y14;
    public $y15;
    public $y16;
    public $y17;
    public $y18;
    public $y19;
    public $y20;
    public $y21;
    public $y22;
    public $y23;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'incoth';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'time', 'income', 'pttype', 'paidst', 'rcptno', 'rcptamt', 'recno', 'cgd', 'codecheck', 'ln'], 'required'],
            [['vn', 'an', 'time', 'rcptno', 'recno', 'ln'], 'integer'],
            [['date'], 'safe'],
            [['rcptamt'], 'number'],
            [['income', 'pttype'], 'string', 'max' => 2],
            [['paidst'], 'string', 'max' => 1],
            [['cgd'], 'string', 'max' => 5],
            [['codecheck'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'an' => 'An',
            'date' => 'Date',
            'time' => 'Time',
            'income' => 'หมวดค่าใช้จ่าย',
            'pttype' => 'สิทธิ์',
            'paidst' => 'Paidst',
            'rcptno' => 'เลขที่ใบเสร็จ',
            'rcptamt' => 'ค่าใช้จ่าย',
            'recno' => 'Recno',
            'cgd' => 'รหัสกรมบัญชีกลาง',
            'codecheck' => 'Codecheck',
            'ln' => 'Ln',
            'id' => 'ID',
        ];
    }

    public function getIncome()
    {
        return $this->hasOne(Income::className(),['costcenter' => 'income']);
    }

    public function getOvst()
    {
        return $this->hasOne(Ovst::className(),['vn' => 'vn']);
    }

    public function getPttype()
    {
        return $this->hasOne(Pttype::className(),['pttype' => 'pttype']);
    }

    public static function getVisitdate($date,$format)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($date);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        if($format == 'short'){
            $result = $d.' '.$m.' '.$y;            
        }
        if($format == 'day'){
            $result = $d.' '.$m;            
        }
        if($format == 'long'){
            $result = $d.' '.$m.' พ.ศ.'.$y;            
        }
        if($format == 'month'){
            $result = $m.' '.$y;            
        }

        return $result;
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_ip_visit".
 *
 * @property int|null $yearbudget
 * @property int $people
 * @property int $admit
 * @property float|null $losd
 * @property int|null $days
 * @property float|null $bed_rate
 */
class ReportIpVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_ip_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget', 'people', 'admit', 'days'], 'integer'],
            [['losd', 'bed_rate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'people' => 'People',
            'admit' => 'Admit',
            'losd' => 'Losd',
            'days' => 'Days',
            'bed_rate' => 'Bed Rate',
        ];
    }
}

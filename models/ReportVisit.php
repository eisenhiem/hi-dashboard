<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "report_visit".
 *
 * @property int|null $m
 * @property int $hn
 * @property int $total

 */
class ReportVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['m', 'hn', 'total'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'm' => 'month',
            'hn' => 'hn',
            'total' => 'total',
        ];
    }

    public static function getVisitdate($date,$format)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($date);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        if($format == 'short'){
            $result = $d.' '.$m.' '.$y;            
        }
        if($format == 'day'){
            $result = $d.' '.$m;            
        }
        if($format == 'long'){
            $result = $d.' '.$m.' พ.ศ.'.$y;            
        }
        if($format == 'month'){
            $yb = $y - 2500;
            $result = $m.' '.$yb;            
        }

        return $result;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_admit".
 *
 * @property string|null $m
 * @property int $total
 * @property int $hn
 */
class ReportAdmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_admit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'hn'], 'integer'],
            [['m'], 'string', 'max' => 69],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'm' => 'M',
            'total' => 'Total',
            'hn' => 'Hn',
        ];
    }
}

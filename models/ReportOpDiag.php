<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_op_diag".
 *
 * @property string $cgroup
 * @property string|null $e_name
 * @property string|null $icd10
 * @property string|null $t_name
 * @property int $total
 */
class ReportOpDiag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_op_diag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cgroup'], 'required'],
            [['total'], 'integer'],
            [['cgroup'], 'string', 'max' => 3],
            [['e_name', 't_name'], 'string', 'max' => 200],
            [['icd10'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cgroup' => 'Cgroup',
            'e_name' => 'E Name',
            'icd10' => 'Icd10',
            't_name' => 'T Name',
            'total' => 'Total',
        ];
    }
}

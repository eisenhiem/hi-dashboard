<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $role;
    public $fullname;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'Lsk27976',
            'fullname' => 'jakkrapong wongkamalasai',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
            'role' => 'admin',
        ],
        '1' => [
            'id' => '1',
            'username' => 'veeraya',
            'password' => '27976',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
            'role' => 'user',
            'fullname' => 'Veeraya',
        ],
        '2' => [
            'id' => '2',
            'username' => 'tuta',
            'password' => '27976',
            'authKey' => 'test102key',
            'accessToken' => '102-token',
            'role' => 'user',
            'fullname' => 'Watchara',
        ],
        '3' => [
            'id' => '3',
            'username' => 'head',
            'password' => '27976',
            'authKey' => 'test100key',
            'accessToken' => '103-token',
            'role' => 'head',
            'fullname' => 'Patchara',
        ],
        '4' => [
            'id' => '4',
            'username' => 'auth',
            'password' => '27976',
            'authKey' => 'test100key',
            'accessToken' => '104-token',
            'role' => 'autherize',
            'fullname' => 'Tay',
        ],
    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}

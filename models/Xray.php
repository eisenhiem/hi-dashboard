<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "xray".
 *
 * @property string $xrycode
 * @property int $unit
 * @property int $film_xray
 * @property string $xryname
 * @property string $ptright
 * @property int $pricexry
 * @property int $pricexrycgd
 * @property string $cgd
 * @property string $etype
 * @property string $chkshow
 * @property string $remark
 * @property string $icd9cm
 */
class Xray extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xray';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['xrycode', 'unit', 'film_xray', 'xryname', 'ptright', 'pricexry', 'pricexrycgd', 'cgd', 'etype'], 'required'],
            [['unit', 'film_xray', 'pricexry', 'pricexrycgd'], 'integer'],
            [['remark'], 'string'],
            [['xrycode'], 'string', 'max' => 4],
            [['xryname'], 'string', 'max' => 30],
            [['ptright'], 'string', 'max' => 200],
            [['cgd'], 'string', 'max' => 5],
            [['etype'], 'string', 'max' => 100],
            [['chkshow'], 'string', 'max' => 1],
            [['icd9cm'], 'string', 'max' => 7],
            [['xrycode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'xrycode' => 'Xrycode',
            'unit' => 'Unit',
            'film_xray' => 'Film Xray',
            'xryname' => 'Xryname',
            'ptright' => 'Ptright',
            'pricexry' => 'Pricexry',
            'pricexrycgd' => 'Pricexrycgd',
            'cgd' => 'Cgd',
            'etype' => 'Etype',
            'chkshow' => 'Chkshow',
            'remark' => 'Remark',
            'icd9cm' => 'Icd9cm',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_op_visit".
 *
 * @property int|null $yearbudget
 * @property int $people
 * @property int $visit
 * @property int $visit_dent
 * @property int $visit_er
 * @property int $visit_opd
 */
class ReportOpVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_op_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget', 'people', 'visit', 'visit_dent', 'visit_er', 'visit_opd'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'people' => 'People',
            'visit' => 'Visit',
            'visit_dent' => 'Visit Dent',
            'visit_er' => 'Visit Er',
            'visit_opd' => 'Visit Opd',
        ];
    }
}

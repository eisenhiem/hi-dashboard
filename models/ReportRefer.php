<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_refer".
 *
 * @property int|null $yearbudget
 * @property int $total
 * @property int $ambulance
 * @property int $ipd
 * @property int $opd
 */
class ReportRefer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_refer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget', 'total', 'ambulance', 'ipd', 'opd'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'total' => 'Total',
            'ambulance' => 'Ambulance',
            'ipd' => 'Ipd',
            'opd' => 'Opd',
        ];
    }
}

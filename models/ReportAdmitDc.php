<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_admit_dc".
 *
 * @property string|null $m
 * @property int $total
 */
class ReportAdmitDc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_admit_dc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total'], 'integer'],
            [['m'], 'string', 'max' => 69],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'm' => 'M',
            'total' => 'Total',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "optriage".
 *
 * @property int $vn
 * @property string $triage
 * @property string $trauma
 * @property string $ucae
 */
class Optriage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'optriage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'triage', 'trauma', 'ucae'], 'required'],
            [['vn'], 'integer'],
            [['triage', 'trauma', 'ucae'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'triage' => 'Triage',
            'trauma' => 'Trauma',
            'ucae' => 'Ucae',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prscdt".
 *
 * @property int $id
 * @property int $prscno
 * @property string $nameprscdt
 * @property string $meditem
 * @property int $qty
 * @property string $medusage
 * @property int $xdoseno
 * @property string $salerate
 * @property string $charge
 * @property string $rfndamt
 * @property string $statusprin
 * @property string $ed
 * @property string $codeed
 * @property string $prtdttm
 */
class Prscdt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prscdt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prscno', 'nameprscdt', 'meditem', 'qty', 'medusage', 'xdoseno', 'salerate', 'charge', 'rfndamt', 'statusprin', 'ed', 'codeed', 'prtdttm'], 'required'],
            [['prscno', 'qty', 'xdoseno'], 'integer'],
            [['salerate', 'charge', 'rfndamt'], 'number'],
            [['prtdttm'], 'safe'],
            [['nameprscdt'], 'string', 'max' => 50],
            [['meditem'], 'string', 'max' => 7],
            [['medusage'], 'string', 'max' => 10],
            [['statusprin', 'ed', 'codeed'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prscno' => 'Prscno',
            'nameprscdt' => 'Nameprscdt',
            'meditem' => 'Meditem',
            'qty' => 'Qty',
            'medusage' => 'Medusage',
            'xdoseno' => 'Xdoseno',
            'salerate' => 'Salerate',
            'charge' => 'Charge',
            'rfndamt' => 'Rfndamt',
            'statusprin' => 'Statusprin',
            'ed' => 'Ed',
            'codeed' => 'Codeed',
            'prtdttm' => 'Prtdttm',
        ];
    }

    public function getMeditems(){
        return $this->hasOne(Meditem::className(),['meditem' => 'meditem']);
    }

}

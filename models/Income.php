<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "income".
 *
 * @property string|null $income
 * @property string|null $nameinc
 * @property string|null $incomeb
 * @property string|null $nameincb
 * @property string|null $op56
 * @property string $costcenter
 * @property string|null $namecost
 * @property string|null $sso_code
 * @property string|null $namesss_code
 */
class Income extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'income';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['costcenter'], 'required'],
            [['income', 'incomeb', 'op56', 'costcenter'], 'string', 'max' => 4],
            [['nameinc', 'nameincb', 'namecost', 'namesss_code'], 'string', 'max' => 255],
            [['sso_code'], 'string', 'max' => 1],
            [['costcenter'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'income' => 'Income',
            'nameinc' => 'Nameinc',
            'incomeb' => 'Incomeb',
            'nameincb' => 'Nameincb',
            'op56' => 'Op56',
            'costcenter' => 'Costcenter',
            'namecost' => 'Namecost',
            'sso_code' => 'Sso Code',
            'namesss_code' => 'Namesss Code',
        ];
    }
}

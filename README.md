HI Dashboard
Project base on Yii2 Framework
minimun requirement 
- PHP 7.x
- Composer 2.x download https://getcomposer.org/download/
- Database Hi

HI Dashboard
======================================================
Version 1.3 
add View Report Refer Er
Update View Report Visit/Report Admit
add Summary Rreport Refer 
change color of graph 
** file in sql/view_dashboard.sql in version 1.3 **
======================================================
Version 1.2 
add Switch layout 
add data in hi_setup >> sql in folder sql
======================================================
Version 1.1
add Dashboard today status
add view visit_pop
add view report_admit
add view report_admit_dc

======================================================
Version 1.0
source https://hitlab.com/eisenhiem/hi-dashboard.git 
view db in folder sql 
if your his is not HI WIN you can create view by model template

======================================================
Install
======================================================
after clone  
go to project directory 
run this command
------------------------------------------------------
chmod -R 777 runtime/
chmod -R 777 web/assets/
chmod 755 yii
------------------------------------------------------